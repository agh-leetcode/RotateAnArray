public class ArrayRotator {
    public static void main(String[] args) {
        ArrayRotator main = new ArrayRotator();
        main.runTheApp();
    }
    private void runTheApp() {
        int arr[] = new int[]{-1,-100,3,99};
        int k = 2;
        rotate(arr, k);
    }
    /* private void rotate(int[] arr, int k) {
         if(arr==null||k==0||arr.length==0)
             return;
         for(int i=0; i<k; i++){
             int temp = arr[arr.length-1];
             System.arraycopy(arr, 0, arr, 1, arr.length - 1);
             *//*for (int j = arr.length-1; j >0; j--) {
                arr[j]=arr[j-1];
            }*//*
            arr[0]=temp;
        }
    }*/
    public void rotate(int[] arr, int k) {
        int[] a = new int[arr.length];
        for(int i =0; i< arr.length; i++){
            a[(i+k)%arr.length] = arr[i];
        }
        for(int i =0; i < arr.length; i++){
            arr[i] = a[i];
        }
    }
}